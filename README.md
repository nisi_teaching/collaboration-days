# collaboration days

Website containing material for the collaboration days at UCL IT technology

Link to site: https://nisi_teaching.gitlab.io/collaboration-days

# Development usage

You need Python above version 3.5 and pip installed  
If you have problems the documentation for MKDocs is at https://www.mkdocs.org/#installation 

1. Clone the repository `git clone git@gitlab.com:EAL-ITT/collaboration-days.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
4. Start the mkdocs dev server `mkdocs serve`
5. The served page is at http://127.0.0.1:8000/
6. How to add pages etc. is documented at https://www.mkdocs.org/#getting-started
