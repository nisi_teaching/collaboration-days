# Collaboration days evaluation

Please complete the Collaboration days evaluation survey located at:  
[https://forms.gle/otyMmJEGM27LmzLi7](https://forms.gle/otyMmJEGM27LmzLi7)

*The link is not active before the end of Collaboration Days*