# About

This website contains information and exercise instructions for the collaboration days at UCL - IT technology education

Collaboration days are part of the introduction programme in IT technology at UCL. 

Collaboration days is a mixture of team forming, presentation of the IT technology profession, contents of the IT technology education as well as presentations from older IT technology students.

The purpose of collaboration days are to establish relations, student to students, student to lecturers and student to profession.
Participation in collaboration days will give you as a student a good start at your studies and a place to belong - your team.  
You will gain more detailed knowledge about the IT Technology education structure and you will get to learn your fellow students for the next two years.  

During collaboration days there will be presentations from the profession in terms of both collaborating companies and graduated students.
Hopefully this will give you a more clear picture of the IT Technology profession, a profession you are entering and starting to become a part of.

Welcome to collaboration days and welcome to IT Technology !