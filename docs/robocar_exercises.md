# Robocar exercises and specifications

## Components diagram

![Robocar components](./robocar_images/Components.png)

## Arduino Uno microprocessor specifications

* The brains - the CPU - clocked at a whopping 20MHz
* Flash ram - 32KB
* SRAM - 2KB
* Sensing = GPIO - 23x

![Arduino uno](./robocar_images/auno.jpg)

## Sensors attached to robocar

* Front and below – Infrared (3x) and ultrasonic sensor (1x) 
* Can you point them out on the car?
* Rear – IR receiver, USB input (for programming) and bluetooth receiver
* One each - can you find them?


# Exercise 9: Time for teamwork

_duration: 15 minutes_

* We work in our assigned teams
* Pick a logo and a name
* Minimum one member has to have Helloblock installed
* Installation guide: [Helloblock Guide](https://docs.google.com/presentation/d/1APec0TD9CbzAIQSIaI9sULXR8tdB6yx0/edit?usp=sharing&ouid=106328493335884631716&rtpof=true&sd=true)


# Exercise 10: Test drive

_duration: 25 minutes_

* Get the IR remote
* Test drive the car using the remote
* Try the different modes
* Do they function as you believed they would? 

# Exercise 11: Knowledge 

_duration: 20 minutes_

If you paid close attention while testing the modes, the next questions should be easy for you
* How does it turn?
* How does it follow a line?
* How are obstacles avoided?
* Difficult - Why does it not use a camera to avoid obstacles?

**Note your answers in teams and we will review shortly**

# Exercise 12: Drawing a circuit

_duration: 20 minutes_

* On a piece of paper, draw a circuit that looks like a connected road, as seen from above
* Turns can be no sharper than 90 degrees
* You can use more pieces of paper if you want

# Exercise 13: Algorithmic thinking exercise

_duration: 25 minutes_

* Try to use the remote to follow the circuit
* Write down what it does, in steps
* E.g. _go straight, turn right, turn left, hairpin etc..._

# Exercise 14: Programming exercise

_duration: 45 minutes_

* Now you have to program the car
* Try to make the car follow the circuit, sticking to it as much as possible without remote control

### Example blocks

![Loop](./robocar_images/ex1.png)
![Loopy loop](./robocar_images/ex2.png)
![Sport](./robocar_images/ex3.png)
![Motor](./robocar_images/ex4.png)
![Lights and sounds](./robocar_images/ex5.png)
![Sensors](./robocar_images/ex6.png)


## USB Driver

* Required for programming the robocar
* Link: [Sketchy.exe](https://drive.google.com/file/d/1AkH6xY9T6oDAF3lvBPCUBlHHe802rHEy/view?usp=sharing)
* Open and install!

## Preparing HelloBlock

* Run Helloblock as admin (right click -> run as admin)
* In Helloblock, click the ”Add extension” button
* Choose BatCar
* Now a new icon is visible in the menu on the left pane – here are the code blocks for the car


## Upload program to car
* Connect car with USB to computer
* Choose ”Code mode”
* Choose the appropriate COM port 
* Upload, and then this should show: 

![Downloading](./robocar_images/dl.png)


# Exercise 15: Line following exercise

_duration: 45 minutes_

* Get the car to follow a line
* Reconstruct the software on the image at the bottom of this page
* Test the car on the new circuit
* Optional: Use the [line following source code](https://github.com/YahboomTechnology/Arduino-Smart-Bat-Car/blob/master/3.SDK%EF%BC%88Sourcecode%EF%BC%89/For%20Arduino%20IDE/3.Line%20Walking/Line_Walking/Line_Walking.ino) (you need Arduino IDE ) 
You can try to edit this, perhaps if you are good, you can beat the HelloBlock template!

![Blocks](./robocar_images/blocks.png)
