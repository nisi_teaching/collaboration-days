# Introduction

Below is a collection of team building exercises that can be used to establish teams.

# Exercise 1: What we share

*duration: 15 minutes*

Everybody has a hobby or something that they invest a lot of spare time in.  
This exercise is about sharing your interests to see if somebody else has similar interests.

This can be electronics projects, a game you play, a coding project, a sparetime job, why you especially have chosen IT Technology etc.

Go to the padlet linked below and make an entry using the *+* button.

In the entry write:

* Your fullname in the **Subject** field
* Your home country
* One or more interests
* One or more projects you have considered or worked on (doesn't have to be IT related)
* One or more skills you are proud of

Link to padlet [https://padlet.com/nisi2/azq3aak7fx71faez](https://padlet.com/nisi2/azq3aak7fx71faez)  


# Exercise 2: Team importance

*duration: 30 minutes*

In IT some of the most sought after skills are being able to work and communicate with others.
It is important that you know the reasons behind working as a part of a team.

1. Use 10 minutes to read the text at [https://eal-itt.gitlab.io/collaboration-days/why_teams/](https://eal-itt.gitlab.io/collaboration-days/why_teams/)
2. With the person sitting next to you talk about why you find it important to be in a team, regarding the text you just read.  Use 10 minutes each. One persons talks and the other listens while writing notes about the reasons from the other person.

# Exercise 3: Belbin roles

*duration: 25 minutes*

*Dr Meredith Belbin and his team discovered that there are nine clusters of behaviour - these were called ' Belbin Team Roles' (see descriptions below).
Each team needs access to each of the nine Belbin Team Role behaviours to become a high performing team. However, this doesn't mean that every team requires nine people! Most people will have two or three Belbin Team Roles that they are most comfortable with, and this can change over time. Each Belbin Team Role has strengths and weaknesses, and each Team Role has equal importance.* - www.belbin.com 2020

To find out which belbin team roles you fit into follow these steps:

1. Use 15 minutes to read about the 9 different belbin team roles at [https://www.belbin.com/about/belbin-team-roles/](https://www.belbin.com/about/belbin-team-roles/) Take notes about the roles you think you fit in to.
2. Use 5 minutes to review your notes and constrain your role choices to the two most fitting roles.
3. Use 5 minutes to write your two chosen team roles in your padlet entry at [https://padlet.com/nisi2/azq3aak7fx71faez](https://padlet.com/nisi2/azq3aak7fx71faez)

# Exercise 4: Elevator pitch part 1

*duration: 30 minutes*

1. Use 5 minutes to quietly consider how you can present yourself in 30-60 seconds, write notes while thinking
2. Use 5 minutes to edit your pitch to only include the most important parts.
3. Use 5 minutes to quietly rehearse your pitch
4. In groups of 8-10 students (211 + 212 class can not be mixed), take turns presenting yourself using your pitch

Contents of your elevator pitch:

* Who are you ? (Name and age)
* What have you done previously regarding school, job, other studies etc. ?
* What motivates you towards the IT Technology education ?

Constraints of the pitch:

* Keep your presentation short and simple in order for everybody to understand it.
* Be personal, engaged, authentic and honest
* The duration of your presentation may not exceed 1 minute!

# Exercise 5: Elevator pitch part 2

*duration: 15 minutes*

* Make new groups of 8-10 students (211 + 212 class can not be mixed)
* Take turns presenting yourself using your pitch

# Exercise 6: Team forming

*duration: 45 minutes*

Rules for forming teams 

1. Each team needs access to each of the nine Belbin Team Role behaviours to become a high performing team. However, this doesn't mean that every team requires nine people! Most people will have two or three Belbin Team Roles that they are most comfortable with

2. Teams can not be mixed between 211 and 212 class

**Exercise instructions**

1. Start by gathering in groups of 8. 
2. In the group use 1 minute each to present your belbin team roles, one person in the group notes the belbin team roles represented in the group.  

3. If you do not cover all 9 belbin team roles note which roles you are missing and notify a teacher.

4. If the group covers all 9 belbin team roles you have a team and can continue with the exercise. 

5. Make a shared document that everybody in the team can access

6. In the team take turns to answer the below questions, one person takes notes in the shared document:  
How many hours a week can I invest in the team work ?  
What priority does team work have for me on a scale from 1 - 10 ?  
What are your strengths ?  
What do you ecpect from the team ?  
How do you prefer to handle disagreements in the team ?  
What are your experiences working in a team ?  
What do you find hard when working together with others ?  
What can't you accept when working with others ?  
What is your dream job after completing the study ?  

# Exercise 7: Team contract

*duration: 45 minutes*

In this exercise you create a team contract, what you include is up to you and you can use the below as an example.
Remember to take notes in your shared document.  

Please note that UCL has standards for contact and behaviour that you are not allowed to violate in your team contract:  [https://www.myucl.dk/student-services/standards-for-contact-and-behaviour](https://www.myucl.dk/student-services/standards-for-contact-and-behaviour)


**Team Contract**

Norms, rules and procedures for our team.

* Notify the team in due time if you are delayed or absent
* The team will meet during school hours
* If we are flexible team work will be easier
* Agreed appointments and deadlines must be met by all team members
* We have confidentiality within the team and do not slander about other team members
* Our meetings always have an agenda which is facilitated by one of the team members
* We all have a responsibility that the agenda is fulfilled at meetings
* We start and end team meetings with an update/recap
* We write minutes of meetings everytime we meet
* We use the agreed sharing platform (google etc.) for minutes of meetings and shared study material
* We work seriously but also allow space for fun   
* We support each other and accept each others differences
* There are no stupid questions or ideas
* Be honest!
* Work concentrated towards the goal and with high work ethic
* Don't judge yourself, you are good enough!
* Good team work is flexibility, mutual respect and recognition, structured work and a contructive attitude

While talking please consider the below:

* How can we support and retain all members of the team throughout the study ?
* What the goal of the team work is ?
* How each team member can contribute to fullfill the team goals ?
* How can we work together with tasks and relations within the team ?

# Exercise 8: Communication and meetings

*duration: 30 minutes*

In your team use 30 minutes to discuss the below questions. Remember to take notes in your shared document.

1. How often we are going to meet (minimum is 2 hours once every 2 weeks)
2. How are you going to communicate outside of meetings ?
3. What is important to communicate about outside of meetings
4. How will you share study material, notes etc. Examples: Google drive, Gitlab, dropbox....
5. A plan for taking turns in doing minutes of meetings and facilitating meetings.
